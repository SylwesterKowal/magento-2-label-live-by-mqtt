# Mage2 Module Kowal LabelLiveByMQTT

    ``kowal/module-labellivebymqtt``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Label LIVE By MQTT

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_LabelLiveByMQTT`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-labellivebymqtt`
 - enable the module by running `php bin/magento module:enable Kowal_LabelLiveByMQTT`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - server (labellive/settings/server)

 - port (labellive/settings/port)

 - username (labellive/settings/username)

 - password (labellive/settings/password)

 - clean_session (labellive/settings/clean_session)


## Specifications




## Attributes



