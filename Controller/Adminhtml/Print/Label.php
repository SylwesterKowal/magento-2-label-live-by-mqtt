<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\LabelLiveByMQTT\Controller\Adminhtml\Print;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;

use \PhpMqtt\Client\MqttClient;
use \PhpMqtt\Client\ConnectionSettings;

class Label implements HttpPostActionInterface
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Json
     */
    protected $serializer;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var Http
     */
    protected $http;

    /**
     * Constructor
     *
     * @param PageFactory $resultPageFactory
     * @param Json $json
     * @param LoggerInterface $logger
     * @param Http $http
     */
    public function __construct(
        PageFactory        $resultPageFactory,
        Json               $json,
        LoggerInterface    $logger,
        Http               $http,
        MqttClient         $mqttClient,
        ConnectionSettings $connectionSettings
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->serializer = $json;
        $this->logger = $logger;
        $this->http = $http;
        $this->mqttClient = $mqttClient;
        $this->connectionSettings = $connectionSettings;
    }

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        try {

            $server   = '192.168.99.106';
            $port     = 1883;
            $clientId = rand(5, 15);
            $username = 'emqx_user';
            $password = 'public';
            $clean_session = false;
            $mqtt_version = MqttClient::MQTT_3_1_1;

            $connectionSettings = (new ConnectionSettings)
                ->setUsername($username)
                ->setPassword($password)
                ->setKeepAliveInterval(60)
                ->setLastWillTopic('emqx/test/last-will')
                ->setLastWillMessage('client disconnect')
                ->setLastWillQualityOfService(1);


            $mqtt = new MqttClient($server, $port, $clientId, $mqtt_version);

            $mqtt->connect($connectionSettings, $clean_session);



            return $this->jsonResponse("client connected\n");
        } catch (LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return ResultInterface
     */
    public function jsonResponse($response = '')
    {
        $this->http->getHeaders()->clearHeaders();
        $this->http->setHeader('Content-Type', 'application/json');
        return $this->http->setBody(
            $this->serializer->serialize($response)
        );
    }
}